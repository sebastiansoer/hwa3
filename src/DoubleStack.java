import java.util.LinkedList;

public class DoubleStack {

   private LinkedList<Double> lst;

   public static void main (String[] argum) throws CloneNotSupportedException {
      DoubleStack stack1 = new DoubleStack();
      stack1.push(1);
      stack1.push(2);
      DoubleStack stack2 = new DoubleStack();
      stack2 = (DoubleStack)stack1.clone();
     /* System.out.println("stack1: " + stack1);
      System.out.println("stack2: " + stack2);
      System.out.println(stack1.equals(stack2));
      System.out.println("##############");
      System.out.println(3. + 3.);
      System.out.println("##############");*/
      System.out.println(interpret("2 5 SWAP -"));
      System.out.println(interpret("2 5 9 ROT - +"));
      System.out.println(interpret("2 5 9 ROT + SWAP -"));
   }

   DoubleStack() {
      lst = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      try {
         DoubleStack lst2 = new DoubleStack();
         for(int i=lst.size() - 1; i>= 0; i--) {
            lst2.push(lst.get(i));
         }
         return lst2;
      } catch (Exception e) {
         throw new CloneNotSupportedException();
      }
   }

   public boolean stEmpty() {
      if (lst.isEmpty()) {
         return true;
      }
      return false;
   }

   public void push (double a) {
      lst.addFirst(a);
   }

   public double pop() {
      if (lst.size() <= 0) {
         throw new RuntimeException("Stack is already empty!");
      }
      return lst.removeFirst();
   }

   public void op (String s) {
      if (lst.size() < 2) {
         throw new RuntimeException("To few numbers in stack!");
      }
      if (!isAritmetic(s)) {
         throw new RuntimeException(s + "is not a valid input!");
      }
      Double op2 = pop(); //a
      Double op1 = pop(); //b
      if (s.equals ("+")) push (op1 + op2);
      if (s.equals ("-")) push (op1 - op2);
      if (s.equals ("*")) push (op1 * op2);
      if (s.equals ("/")) push (op1 / op2);
      if (s.equals ("SWAP")) {
         push(op2);
         push(op1);
      }
      if (s.equals ("ROT")) {
         if (lst.size() < 1) {
            throw new RuntimeException("To few numbers in stack!");
         }
         Double op3 = pop(); //c
         push(op1);
         push(op2);
         push(op3);
      }
   }
  
   public double tos() {
      if (lst.size() <= 0) {
         throw new RuntimeException("Stack is empty!");
      }
      return lst.get(0);
   }

   @Override
   public boolean equals (Object o) {
      if(((DoubleStack) o).lst.size() != lst.size()) return false;
      if(stEmpty()) return true;
      for (int i=0;i<=lst.size() -1; i++) {
         if(!((DoubleStack)o).lst.get(i).equals(lst.get(i))) return false;
      }
      return true;
   }

   @Override
   public String toString() {
      if(stEmpty()) return "Empty";
      StringBuilder builder = new StringBuilder();
      for (int i = lst.size()-1; i >= 0 ; i--) {
         builder.append(lst.get(i)).append(" ");
      }
      return builder.toString();
   }

   public static double interpret (String pol) {
      String[] polished = pol.trim().split("\\s+");
      DoubleStack stack = new DoubleStack();
      if(pol.equals(0) || pol.isEmpty()) {
         throw new RuntimeException(pol + " is empty!");
      }

      for (String s: polished) {
         if(isNumeric(s)) {
            stack.push(Double.parseDouble(s));
         } else if (isAritmetic(s)) {
            if (stack.lst.size() < 2) {
               throw new RuntimeException("Too few numbers in stack");
            }
            stack.op(s);
         } else {
            throw new RuntimeException(s + "is not a valid input from" + pol);
         }
      }
      if(stack.lst.size() > 1) {
         throw new RuntimeException("Too many inputs in " + pol);
      }
      return stack.tos();
   }

   private static boolean isNumeric(String s) {
      try {
         Double d = Double.parseDouble(s);
      } catch (NumberFormatException e) {
         return false;
      }
      return true;
   }

   private static boolean isAritmetic(String s) {
      if (s.equals("*") || s.equals("/") || s.equals("+") || s.equals("-") || s.equals("SWAP") || s.equals("ROT")) {
         return true;
      }
      return false;
   }


}

